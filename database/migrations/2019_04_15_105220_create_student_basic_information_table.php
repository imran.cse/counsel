<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentBasicInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_basic_information', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name',120)->nullable();
            $table->string('father_name',120)->nullable();
            $table->string('mother_name',120)->nullable();
            $table->enum('sex',['m','f'])->nullable();
            $table->string('phone',13);
            $table->string('email',100)->nullable();

            $table->string('last_education',100)->nullable();
            $table->string('profession',100)->nullable();
            $table->integer('age')->nullable();
            $table->string('division',50)->nullable();
            $table->string('area',50)->nullable();

            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('departments');



            $table->unsignedBigInteger('course_id');
            $table->foreign('course_id')->references('id')->on('courses');


            $table->string('how_they_know',50)->nullable();
            $table->string('counseling_by',100)->nullable();
            $table->string('type')->nullable();

            $table->timestamps();
        });






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_basic_information');
    }
}
