<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAcademicInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_academic_informations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('student_basic_information');

            $table->string('exam_name');
            $table->string('group')->nullable();
            $table->string('board')->nullable();
            $table->string('passing_year');
            $table->integer('roll_of_exam');
            $table->string('grade');
            $table->string('picture')->nullable();
            $table->string('ssc_marksheet')->nullable();
            $table->string('nid_card')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_academic_informations');
    }
}
