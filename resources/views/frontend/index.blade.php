

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      Online Admission | DPI
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="{{asset('frontend/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('frontend/assets/css/now-ui-kit.css?v=1.3.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('frontend/assets/demo/demo.css')}}" rel="stylesheet"/>
</head>

<body class="login-page sidebar-collapse">
 
  <div class="page-header clear-filter">
    <div class="page-header-image" style="width: 100%;background: #ffffff"></div>
    <!-- <div class="content"> -->
      <div class="container">
        <div class="col-md-12 ml-auto mr-auto">
          <div class="row">
            <div class="col-md-2">
              <a href="#"> <img src="{{asset('frontend/assets/img/dpi.png')}}"></a>
            </div>

            <div class="col-md-8">
              <br>
              <h2>Online Admission Form</h2>
            </div>
          </div>
          
          <div class="card card-login card-plain">


            <form class="form" method="post" action="{{ route('online-admission.store') }}">
                @csrf
              
              <div class="card-body">
                  <div class="row">
                     <div class="col-md-6">
                     	<h4>Personal Information</h4>
                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                               <i class="now-ui-icons education_hat"></i>
                             </span>
                           </div>


                           <select name="technology" class="form-control" style="color: #ffffff;background: rgb(87,87,87);">
                              <option selected="" disabled="" value="">Select Your Technology</option>

                              @foreach($courses as $course)
                                   <option value="{{$course->id}}">{{$course->course_name}}</option>
                              @endforeach

                           </select>
                     
                        </div>

                        <div class="input-group no-border input-lg" >
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons users_circle-08"></i>
                             </span>
                           </div>
                           <input name="name" type="text" class="form-control" placeholder="Name of the Applicant">
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons users_circle-08"></i>
                             </span>
                           </div>
                           <input name="father_name" type="text" placeholder="Father's Name" class="form-control" />
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons users_circle-08"></i>
                             </span>
                           </div>
                           <input name="mother_name" type="text" placeholder="Mother's Name" class="form-control" />
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons ui-1_calendar-60"></i>
                             </span>
                           </div>
                           <input name="date_of_birth" type="date" class="form-control" placeholder="Date Of Birth">
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons ui-1_email-85"></i>
                             </span>
                           </div>
                           <input name="email" type="email" placeholder="Email Address" class="form-control" />
                        </div>

                        <div class="input-group no-border input-lg">
                           
                          <textarea name="permanent_address" class="form-control" rows="4" cols="50" placeholder="Permanent Address*"></textarea>
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons tech_mobile"></i>
                             </span>
                           </div>
                           <input name="phone" type="text" placeholder="Phone Number" class="form-control"/>
                        </div>
                        <br>

                          <h4>Academic Information</h4>
                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons users_circle-08"></i>
                             </span>
                           </div>
                           <select name="exam_name" class="form-control" style="color: #ffffff;background: rgb(87,87,87);">
                              <option selected="" disabled="" value="">Name of Exam</option>
                               <option value="ssc">SSC</option>
                               <option value="dakhil">Dakhil</option>
                               <option value="vocational">Vocational</option>
                           </select>
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons education_paper"></i>
                             </span>
                           </div>
                           <input name="group_name" type="text" placeholder="Group/Subject ex: Science" class="form-control" />
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons education_atom"></i>
                             </span>
                           </div>
                           <input name="board_name" type="text" placeholder="Board/University" class="form-control" />
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons education_hat"></i>
                             </span>
                           </div>
                           <select name="passing_year" class="form-control" style="color: #ffffff;background: rgb(87,87,87);">
                              <option selected="0" disabled="" value="">Year of Passing</option>
                               <option value="2019">2019</option>
                               <option value="2018">2018</option>
                               <option value="2017">2017</option>
                               <option value="2016">2016</option>
                               <option value="2015">2015</option>
                               <option value="2014">2014</option>
                               <option value="2013">2013</option>
                               <option value="2012">2012</option>
                               <option value="2011">2011</option>
                               <option value="2010">2010</option>
                               <option value="2009">2009</option>
                               
                           </select>
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons location_world"></i>
                             </span>
                           </div>
                           <input name="roll_of_exam" type="text" placeholder="Roll of Exam" class="form-control" />
                        </div>

                        <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons users_circle-08"></i>
                             </span>
                           </div>
                           <input  name="grade" type="text" placeholder="Class/Division/Grade ex: A / A+" class="form-control" />
                        </div>

                        <br>

                        <h4>Upload Your Document</h4>

                         <div class="input-group no-border input-lg" style="background: rgb(87,87,87);border-radius: 31px;">
                            <div>

                              <span class="btn btn-raised btn-round btn-default btn-file" style="background: none">
                                 <i class="now-ui-icons arrows-1_cloud-upload-94"></i>
                             </span>
                              
                              <span class="fileinput-new">Marksheet  </span>
                              
                              <input type="file" name="upload_marksheet" /></span>
                                
                            </div>
                        </div>
                        <!-- <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons design_image"></i>
                             </span>
                           </div>
                           <input name="upload_image" type="file" placeholder="Upload Your Image" class="form-control" />
                        </div> -->

                       <!--  <div class="input-group no-border input-lg">
                           <div class="input-group-prepend">
                             <span class="input-group-text">
                                 <i class="now-ui-icons arrows-1_cloud-upload-94"></i>
                             </span>
                           </div>
                           <input name="upload_marksheet" type="file" placeholder="Upload Your SSC Marksheet" class="form-control" />
                        </div>
 -->
                        <div class="input-group no-border input-lg" style="background: rgb(87,87,87);border-radius: 31px;">
                            <div>

                              <span class="btn btn-raised btn-round btn-default btn-file" style="background: none">
                                 <i class="now-ui-icons arrows-1_cloud-upload-94"></i>
                             </span>
                              
                              <span class="fileinput-new">NID Card</span>
                              
                              <input type="file" name="upload_nid" /></span>
                                
                            </div>
                        </div>


                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                          <div class="fileinput-new thumbnail img-raised">
                             <a href="#"> <img src="{{asset('frontend/assets/img/up_image.png')}}"></a>
                          </div>
                           <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                          <div>
                              <span class="btn btn-raised btn-round btn-default btn-file" style=" background: rgb(87,87,87);width: 100%;">
                                  <span class="fileinput-new">Upload Image</span>
                            
                                  <input type="file" name="upload_image" />
                              </span>
                             
                          </div>
                        </div>

                        <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Submit</button>

                     </div>
                 

                     <!-- <div class="col-md-6">
                        <br>
                        <h3>Payment Information</h3>
                        <h6>Bkash Account No. 01708549215</h6>
                        <h6>বিকাশে টাকা পাঠালে রেফারেন্সে অবশ্যই আবেদনকারীর SSC Roll নাম্বার দিতে হবে।</h6>
                     </div> 
 -->

                  
               </div>   
            </form>
            </div>
          </div>
        </div>
      <!-- </div> -->
    </div>
    <footer class="footer">
      <div class="container">

        <div  class="" id="copyright">
          &copy;
          Developed by
          <a href="https://www.invisionapp.com" target="_blank">BSDI Web Team</a>
        </div>


      </div>
    </footer>
  </div>
  <!--   Core JS Files   -->
  <script src="{{asset('frontend/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('frontend/assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('frontend/assets/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{asset('frontend/assets/js/plugins/bootstrap-switch.js')}}"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{asset('frontend/assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="{{asset('frontend/assets/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('frontend/assets/js/now-ui-kit.js?v=1.3.0')}}" type="text/javascript"></script>
</body>

</html>
