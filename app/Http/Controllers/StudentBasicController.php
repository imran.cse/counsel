<?php

namespace App\Http\Controllers;

use App\StudentBasic;
use Illuminate\Http\Request;

class StudentBasicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentBasic  $studentBasic
     * @return \Illuminate\Http\Response
     */
    public function show(StudentBasic $studentBasic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentBasic  $studentBasic
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentBasic $studentBasic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentBasic  $studentBasic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentBasic $studentBasic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentBasic  $studentBasic
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentBasic $studentBasic)
    {
        //
    }
}
