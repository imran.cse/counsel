<?php

namespace App\Http\Controllers;

use App\StudentBasicInformation;
use Illuminate\Http\Request;

class StudentBasicInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentBasicInformation  $studentBasicInformation
     * @return \Illuminate\Http\Response
     */
    public function show(StudentBasicInformation $studentBasicInformation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentBasicInformation  $studentBasicInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentBasicInformation $studentBasicInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentBasicInformation  $studentBasicInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentBasicInformation $studentBasicInformation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentBasicInformation  $studentBasicInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentBasicInformation $studentBasicInformation)
    {
        //
    }
}
