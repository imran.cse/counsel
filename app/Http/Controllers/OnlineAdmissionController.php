<?php

namespace App\Http\Controllers;
use App\Department;
use App\Course;
use App\StudentBasicInformation;
use App\StudentAcademic;


use App\StudentInformation;
use Illuminate\Http\Request;

class OnlineAdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $courses = Course::all();

        return view('frontend.index',compact('departments','courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $student_basic_informations = new StudentInformation();
       $student_academic_informations = new StudentAcademic();

       $basic_data = [
           'name' => $request->name,
           'father_name' => $request->father_name,
           'mother_name' => $request->mother_name,
           'sex' => null,
           'phone' => $request->phone,
           'email' => $request->email,
           'last_education' => null,
           'profession' => null,
           'age' => $request->age,
           'division' => null,
           'area' => $request->area,
           'department_id' => 2,
           'course_id' => 1,
           'how_they_know' => null,
           'counseling_by' => null,
           'type' => 'online_admission'
       ];
       //dd($basic_data);

      StudentBasicInformation::create($basic_data);





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
