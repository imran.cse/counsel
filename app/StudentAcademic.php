<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentAcademic extends Model
{
    protected $table='student_academic_informations';
    protected $fillable=['student_id','exam_name','group','board','passing_year','roll_of_exam','grade','picture','ssc_marksheet','nid_card'];




}
