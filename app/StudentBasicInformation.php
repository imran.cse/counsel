<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentBasicInformation extends Model
{
    protected $table="student_basic_information";
    protected $fillable=['id','name','father_name','mother_name','sex','phone','email','last_education','profession','age','division','area','department_id','course_id','how_they_know','counseling_by','type'];

    public function discussions(){
        return $this->hasMany('App\Discussion')->select('feedback','discussion_summary','updated_at');
    }
}
